import { useState } from 'react'
import Alert from '../../components/Alert'
import Banner from '../../components/Banner'
import cls from './style.module.scss'

export default function Main() {
  const [open, setOpen] = useState(false)

  const onOpen = () => {
    setOpen(true)
  }
  const onClose = () => {
    setOpen(false)
  }

  return (
    <main className={cls.container}>
      <Alert onClose={onClose} onOpen={onOpen} />
      <div className={cls.bannerContainer}>
        <Banner open={open} />
      </div>
    </main>
  )
}
