import cls from './style.module.scss'
import image from '../../assets/images/banner.png'
import { IoMdClose } from 'react-icons/io'
import useLocalStorage from '../../hooks/useLocastorage'

export default function Banner({ open }) {
  const [isVisible, setIsVisible] = useLocalStorage('isVisibleBanner', true)

  return (
    <div className={`${cls.banner} ${open && isVisible ? cls.open : ''}`}>
      <button onClick={() => setIsVisible(false)} className={cls.closeBtn}>
        <IoMdClose color='#fff' fontSize='24px' opacity='0.3' />
      </button>
      <div className={cls.image}>
        <img src={image} alt='banner' />
      </div>
      <div className={cls.content}>
        <h2>Black Friday</h2>
        <h3>10%OFF</h3>
        <p className={cls.promocode}>
          Use code{' '}
          <span>
            <b>10FRIDAY</b>
          </span>{' '}
          at checkout
        </p>
        <button>
          Shop now <span>through Monday</span>
        </button>
      </div>
    </div>
  )
}
