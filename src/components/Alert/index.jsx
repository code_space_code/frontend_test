import cls from './style.module.scss'
import image from '../../assets/images/display.png'
import { IoMdClose } from 'react-icons/io'
import { useInView } from 'react-intersection-observer'
import { useEffect } from 'react'
import { MdArrowForwardIos } from 'react-icons/md'

export default function Alert({ onClose, onOpen }) {
  const [ref, inView, entry] = useInView()

  useEffect(() => {
    if (entry?.isIntersecting !== undefined) {
      if (inView) {
        onClose()
      } else {
        onOpen()
      }
    }
  }, [entry, inView])

  return (
    <div className={cls.alert} ref={ref}>
      <img alt='test' src={image} className={cls.leftImage} />
      <nav>
        <ul>
          <li>
            <b>Black Friday</b>, 24-27 Nov
          </li>
          <li>
            <b>
              <span>10%OFF</span>
            </b>
          </li>
          <li>
            Use code{' '}
            <b>
              <span>10FRIDAY</span>
            </b>{' '}
            at checkout
          </li>
        </ul>
      </nav>
      <div className={cls.rightElements}>
        <button>Shop now</button>
        <button>
          <IoMdClose color='#fff' fontSize='24px' opacity='0.3' />
        </button>
        <button>
          <MdArrowForwardIos color='#fff' fontSize='20px' opacity='0.3' />
        </button>
      </div>
    </div>
  )
}
